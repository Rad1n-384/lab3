package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void echo() {
        assertEquals("testing echo method", 5, App.echo(5));
    }

    @Test
    public void oneMore() {
        assertEquals("testing oneMore method", 5, App.oneMore(4));
    }
}
